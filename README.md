## Desafio Engenheiro de Dados
<br/>

## RECOMENDAÇÕES

* Recomendamos a utilização do [SQLFiddle](http://sqlfiddle.com), PostgreSQL como engine de SQL, e o arquivo [data.sql](data.sql) para construir a tabela com os seus dados e testar suas consultas. *(Pense em usar CTEs e Window Functions para auxiliar)*

* Para o teste 5 recomendamos o uso do [VSCode](https://code.visualstudio.com/download) em conjunto com a extensão do [Jupyter](https://marketplace.visualstudio.com/items?itemName=ms-toolsai.jupyter) para escrita dos códigos, a solução pode ser feita utilizando [PySpark](https://pypi.org/project/pyspark/) ou [Pandas](https://pypi.org/project/pandas/).

---
<br/>

### **1. DESAFIO DE REPRESENTATIVIDADE DOS ADS**

A tabela abaixo representa "ads" realizados em determinada região e pais. 

| ad_nk | data_nk | regiao_nk | pais_nk |
| --- | --- | --- | --- |
| 1 | 2018-11-06 | LATAM | AR |
| 2 | 2018-11-06 | ASIA | IN |
| 3 | 2018-11-06 | ASIA | IN |
| 4 | 2018-11-06 | LATAM | BR |
| 5 | 2018-11-07 | LATAM | AR |
| 6 | 2018-11-07 | LATAM | AR |
| 7 | 2018-11-07 | ASIA | ID |
| 8 | 2018-11-07 | ASIA | ID |
| 9 | 2018-11-07 | ASIA | IN |
| 10 | 2018-11-07 | LATAM | BR |
| 11 | 2018-11-07 | LATAM | AR |
	 
Escreva uma consulta SQL que retorne os seguintes valores:

1. Total de "ads" por dia, por pais. (A)
2. Total de "ads" na data anterior por pais. (B)
3. A diferença entra (A) e (B).
4. Total de "ads" por dia, por região.
5. O percentual de "ads" por pais/data dentro de sua região.

<br/>

O resultado esperado é:

| data_nk | regiao_nk | pais_nk | Q1 | Q2 | Q3 | Q4 | Q5 |
| --- | --- | --- | --- | --- | --- | --- | --- |
| 2018-11-06 | ASIA | IN | 2 | null | null | 2 | 100.00 |
| 2018-11-06 | LATAM | AR | 1 | null | null | 2 | 50.00 |
| 2018-11-06 | LATAM | BR | 1 | null | null | 2 | 50.00 |
| 2018-11-07 | ASIA | ID | 2 | null | null | 3 | 66.67 |
| 2018-11-07 | ASIA | IN | 1 | 2 | -1 | 3 | 33.33 |
| 2018-11-07 | LATAM | AR | 3 | 1 | 2 | 4 | 75.00 |
| 2018-11-07 | LATAM | BR | 1 | 1 | 0 | 4 | 25.00 |

<br/> 

### **2. EVOLUA O MODELO DE DADOS**
Considere o modelo de vendas abaixo:

![Modelo de Vendas](dw.png)

Os usuários nos reportaram um problema: Quando o cliente altera o estado civil na plataforma transacional, representada pela coluna  ***MaritalStatus*** na tabela ***Customer*** no modelo multi-dimensional , essa atualização é refletida para todos os registros anteriores de vendas (tabela ***Sales***) no dashboard de vendas. 

Como podemos resolver isto alterando o modelo?

<br/>

### **3. PROBLEMA: TROCA DE LUGARES**
Mary é professora de uma escola e tem um assento na mesa para armazenar os nomes dos alunos e os respectivos IDs de assento. O ID da coluna é um incremento contínuo. Mary quer trocar de lugar para os alunos adjacentes.

Você pode escrever uma consulta SQL para gerar o resultado para Mary?

|    id   | student |
|   ---   |   ---   |
|    1    | Abbot   |
|    2    | Doris   |
|    3    | Emerson |
|    4    | Green   |
|    5    | Jeames  |

<br/>

O resultado esperado é:

|    id   | student |
|   ---   |   ---   |
|    1    | Doris   |
|    2    | Abbot   |
|    3    | Green   |
|    4    | Emerson |
|    5    | Jeames  |


**Nota: Se o número de alunos for ímpar, não há necessidade de alterar o assento do último aluno.**

<br/>

### **4. PROBLEMA: SALÁRIO MAIS ALTO DO DEPARTAMENTO**
A tabela ***Employee*** contém todos os funcionários. Todo funcionário tem um ID, um salário e também há uma coluna para o ID do departamento.

| Id  | Name  | Salary | DepartmentId |
| --- | ---   | ---    | ---          |
| 1   | Joe   | 70000  | 1            |
| 2   | Jim   | 90000  | 1            |
| 3   | Henry | 80000  | 2            |
| 4   | Sam   | 60000  | 2            |
| 5   | Max   | 90000  | 1            |

A tabela Departamento contém todos os departamentos da empresa.

| Id  | Name     |
| --- | ---      |
| 1   | IT|
| 2   | Sales    |

Escreva uma consulta SQL para encontrar funcionários com o salário mais alto em cada um dos departamentos. Para as tabelas acima, sua consulta SQL deve retornar as seguintes linhas *(a ordem das linhas não importa)*.
<br>
**OBS:** para essa questão de SQL, por favor, **NÃO** utilizar a função MAX.


<br/>

Saída esperada:

| Department | Employee | Salary |
| ---        | ---      | ---    |
| IT         | Max      | 90000  |
| IT         | Jim      | 90000  |
| Sales      | Henry    | 80000  |

<br/>

### **5. PROBLEMA: REGISTROS QUE SOFRERAM MUDANÇA**
Com base nos arquivos [usuarios_OLD.csv](usuarios_OLD.csv) e [usuarios_LAST.csv](usuarios_LAST.csv), calcule a coluna *ATUALIZADO_EM*, que comparando os dados dos 2 arquivos sinalize a data em que foi percebida a mudança. EX.: Momento de execução do script.

Se não houver mudança o valor da coluna *ATUALIZADO_EM*, deverá ser NULO.


O resultado esperado é:

|ID |CPF           |NOME                      |EMAIL                         |ESTADO_CIVIL|ATUALIZADO_EM          |
|---|--------------|--------------------------|------------------------------|------------|-----------------------|
|1  |911.660.770-80|Lara Moura Souza          |lara.souza@localhost.com      |Solteiro(a) |null                   |
|2  |230.326.180-53|Bianca Rocha Dias         |bianca.dias@localhost.com     |Viúvo(a)    |null                   |
|3  |960.183.650-06|Lívia Oliveira Cardoso    |livia.cardoso@localhost.com   |Casado(a)   |2022-05-11 17:48:25.296|
|4  |260.658.870-30|Beatriz Barros Gomes      |beatriz.gomes@localhost.com   |Casado(a)   |2022-05-11 17:48:25.296|
|5  |927.027.290-70|Itamar Franco             |itamar.franco@localhost.com   |Casado(a)   |2022-05-11 17:48:25.296|
|6  |268.552.470-32|Regina Alvez Prado        |regina.prado@localhost.com    |Viúvo(a)    |2022-05-11 17:48:25.296|
|7  |797.423.600-06|Catarina Martins Martins  |catarina.martins@localhost.com|Solteiro(a) |null                   |
|8  |368.942.710-03|Steve Magal               |steve.magal@localhost.com     |Solteiro(a) |2022-05-11 17:48:25.296|
|9  |600.361.190-16|Sidney Seagal             |sidney.seagal@localhost.com   |Casado(a)   |2022-05-11 17:48:25.296|
|10 |256.273.220-04|Ana Catarina              |ana.catarina@localhost.com    |Solteiro(a) |2022-05-11 17:48:25.296|
|11 |488.273.280-76|Dona Danuza               |dona.danuza@localhost.com     |Casado(a)   |2022-05-11 17:48:25.296|
|12 |678.964.500-46|Seu Edson                 |seu.edson@localhost.com       |Casado(a)   |2022-05-11 17:48:25.296|
|13 |062.959.670-00|Vovó Juju                 |vovo.juju@localhost.com       |Viúvo(a)    |2022-05-11 17:48:25.296|
|14 |605.910.210-72|Vovó Gigi                 |vovo.gigi@localhost.com       |Viúvo(a)    |2022-05-11 17:48:25.296|
|15 |928.381.540-87|Vicente Van Gogh          |vicente.gogh@localhost.com    |Solteiro(a) |2022-05-11 17:48:25.296|
|16 |110.037.790-50|Gil Brother               |gil.brother@localhost.com     |Viúvo(a)    |2022-05-11 17:48:25.296|
|17 |980.632.960-01|Rebeca Lima Gomes         |rebeca.gomes@localhost.com    |Solteiro(a) |null                   |
|18 |114.947.980-93|Irmão do Jorel            |irmao.jorel@localhost.com     |Solteiro(a) |2022-05-11 17:48:25.296|
|19 |042.595.660-16|James Hetfield            |james.hetfield@localhost.com  |Casado(a)   |2022-05-11 17:48:25.296|
|20 |336.570.710-75|Pedro Paulo Soares Pereira|pedro.pereira@localhost.com   |Casado(a)   |2022-05-11 17:48:25.296|

---
<br/>

## **COMO ENTREGAR**
Esperamos receber um arquivo através do email com o seu nome (ex.: NomeSobrenome.zip) contendo as respostas do desafio.
